﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudManager : MonoBehaviour {
    // Start is called before the first frame update

    void OnDrawGizmos () {

        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = Color.blue;
        Gizmos.DrawCube (Vector3.zero, Vector3.one);
        Gizmos.DrawLine (Vector3.zero, Vector3.forward);
    }
}