﻿using System.IO;
using EditorGUITable;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (CustomTerrain))]
[CanEditMultipleObjects]

public class CustomTerrainEditor : Editor {

    //properties -----------
    string filename = "mySavedHeightmapTexture";
    SerializedProperty randomHeightRange;
    SerializedProperty heightMapScale;
    SerializedProperty heightMapImage;
    SerializedProperty perlinXScale;
    SerializedProperty perlinYScale;
    SerializedProperty perlinOffsetX;
    SerializedProperty perlinOffsetY;
    SerializedProperty perlinOctaves;
    SerializedProperty perlinPersistance;
    SerializedProperty perlinHeightScale;
    SerializedProperty resetTerrain;
    SerializedProperty voronoiFallOff;
    SerializedProperty voronoiDropOff;
    SerializedProperty voronoiMinHeight;
    SerializedProperty voronoiMaxHeight;
    SerializedProperty voronoiPeaks;
    SerializedProperty voronoiType;

    SerializedProperty MPDheightMin;
    SerializedProperty MPDheightMax;
    SerializedProperty MPDheightDampenerPower;
    SerializedProperty MPDroughness;

    SerializedProperty smoothAmount;

    GUITableState splatMapTable;
    SerializedProperty splatHeights;

    GUITableState vegetationTable;
    SerializedProperty vegetation;
    SerializedProperty maxTrees;
    SerializedProperty treeSpacing;

    GUITableState detailTable;
    SerializedProperty maxDetail;
    SerializedProperty detailSpacing;
    SerializedProperty details;

    SerializedProperty waterHeight;
    SerializedProperty waterGO;
    SerializedProperty shoreLineMaterial;

    GUITableState perlinParameterTable;
    SerializedProperty perlinParameters;

    // SerializedProperty splatNoiseXScale;
    //SerializedProperty splatNoiseYScale;
    //SerializedProperty splatNoisescaler;
    //SerializedProperty splatOffset;

    SerializedProperty erosionType;
    SerializedProperty erosionStrength;
    SerializedProperty springsPerRiver;
    SerializedProperty solubility;
    SerializedProperty droplets;
    SerializedProperty erosionSmoothAmount;
    SerializedProperty erosionAmount;
    SerializedProperty WindDir;

    SerializedProperty numberOfClouds;
    SerializedProperty particlesPerCloud;
    SerializedProperty cloudParticleSize;
    SerializedProperty cloudSizeMin;
    SerializedProperty cloudSizeMax;
    SerializedProperty cloudMaterial;
    SerializedProperty cloudShadowMaterial;
    SerializedProperty cloudColour;
    SerializedProperty liningColour;
    SerializedProperty cloudMinSpeed;
    SerializedProperty cloudMaxSpeed;
    SerializedProperty cloudDistanceTraveled;

    GUITableState SpawnableObjectsTable;
    SerializedProperty objectSpacing;

    // SerializedProperty townMarker1;
    // SerializedProperty townMarker2;

    //fold outs ------------
    bool showRandom = false;
    bool showLoadHeights = false;
    bool showPerlinNoise = false;
    bool showMultiplePerlin = false;
    bool showVoronoi = false;
    bool showMPD = false;
    bool showSmooth = false;
    bool showSplatMaps = false;
    bool showHeights = false;
    bool showVegetation = false;
    bool showDetails = false;
    bool showWater = false;

    bool showErosion = false;

    bool showObjects = false;
    bool showClouds = false;

    Texture2D hmTexture;

    void OnEnable () {
        randomHeightRange = serializedObject.FindProperty ("randomHeightRange");
        heightMapScale = serializedObject.FindProperty ("heightMapScale");
        heightMapImage = serializedObject.FindProperty ("heightMapImage");
        perlinXScale = serializedObject.FindProperty ("perlinXScale");
        perlinYScale = serializedObject.FindProperty ("perlinYScale");
        perlinOffsetX = serializedObject.FindProperty ("perlinOffsetX");
        perlinOffsetY = serializedObject.FindProperty ("perlinOffsetY");
        perlinOctaves = serializedObject.FindProperty ("perlinOctaves");
        perlinPersistance = serializedObject.FindProperty ("perlinPersistance");
        perlinHeightScale = serializedObject.FindProperty ("perlinHeightScale");
        resetTerrain = serializedObject.FindProperty ("resetTerrain");
        perlinParameterTable = new GUITableState ("perlinParameterTable");
        perlinParameters = serializedObject.FindProperty ("perlinParameters");
        voronoiDropOff = serializedObject.FindProperty ("voronoiDropOff");
        voronoiFallOff = serializedObject.FindProperty ("voronoiFallOff");
        voronoiMinHeight = serializedObject.FindProperty ("voronoiMinHeight");
        voronoiMaxHeight = serializedObject.FindProperty ("voronoiMaxHeight");
        voronoiPeaks = serializedObject.FindProperty ("voronoiPeaks");
        voronoiType = serializedObject.FindProperty ("voronoiType");
        MPDheightMin = serializedObject.FindProperty ("MPDheightMin");
        MPDheightMax = serializedObject.FindProperty ("MPDheightMax");
        MPDheightDampenerPower = serializedObject.FindProperty ("MPDheightDampenerPower");
        MPDroughness = serializedObject.FindProperty ("MPDroughness");
        smoothAmount = serializedObject.FindProperty ("smoothAmount");
        splatMapTable = new GUITableState ("splatMapTable");
        splatHeights = serializedObject.FindProperty ("splatHeights");
        hmTexture = new Texture2D (513, 513, TextureFormat.ARGB32, false);
        vegetationTable = new GUITableState ("vegetationParameterTable");
        vegetation = serializedObject.FindProperty ("vegetation");
        maxTrees = serializedObject.FindProperty ("maxTrees");
        treeSpacing = serializedObject.FindProperty ("treeSpacing");
        details = serializedObject.FindProperty ("details");
        maxDetail = serializedObject.FindProperty ("maxDetail");
        detailSpacing = serializedObject.FindProperty ("detailSpacing");
        detailTable = new GUITableState ("detailTable");
        waterHeight = serializedObject.FindProperty ("waterHeight");
        waterGO = serializedObject.FindProperty ("waterGO");
        shoreLineMaterial = serializedObject.FindProperty ("shoreLineMaterial");
        erosionType = serializedObject.FindProperty ("erosionType");
        erosionStrength = serializedObject.FindProperty ("erosionStrength");
        springsPerRiver = serializedObject.FindProperty ("springsPerRiver");
        solubility = serializedObject.FindProperty ("solubility");
        droplets = serializedObject.FindProperty ("droplets");
        erosionSmoothAmount = serializedObject.FindProperty ("erosionSmoothAmount");
        erosionAmount = serializedObject.FindProperty ("erosionAmount");
        WindDir = serializedObject.FindProperty ("WindDir");
        numberOfClouds = serializedObject.FindProperty ("numberOfClouds");
        particlesPerCloud = serializedObject.FindProperty ("particlesPerCloud");
        cloudParticleSize = serializedObject.FindProperty ("cloudParticleSize");
        cloudSizeMin = serializedObject.FindProperty ("cloudSizeMin");
        cloudSizeMax = serializedObject.FindProperty ("cloudSizeMax");
        cloudMaterial = serializedObject.FindProperty ("cloudMaterial");
        cloudShadowMaterial = serializedObject.FindProperty ("cloudShadowMaterial");
        cloudColour = serializedObject.FindProperty ("cloudColour");
        liningColour = serializedObject.FindProperty ("liningColour");
        cloudMinSpeed = serializedObject.FindProperty ("cloudMinSpeed");
        cloudMaxSpeed = serializedObject.FindProperty ("cloudMaxSpeed");
        cloudDistanceTraveled = serializedObject.FindProperty ("cloudDistanceTraveled");
        objectSpacing = serializedObject.FindProperty ("ObjectSpacing");
        SpawnableObjectsTable = new GUITableState ("SpawnableObjectsTable");
        //townMarker1 = serializedObject.FindProperty ("townMarker1");
        //townMarker2 = serializedObject.FindProperty ("townMarker2");

        // splatNoiseXScale = serializedObject.FindProperty ("splatNoiseXScale");
        // splatNoiseYScale = serializedObject.FindProperty ("splatNoiseYScale");
        // splatNoisescaler = serializedObject.FindProperty ("splatNoisescaler");
        //  splatOffset = serializedObject.FindProperty ("splatOffset");

    }

    Vector2 scrollPos;
    public override void OnInspectorGUI () {
        serializedObject.Update ();

        CustomTerrain terrain = (CustomTerrain) target;

        Rect r = EditorGUILayout.BeginVertical ();
        scrollPos = EditorGUILayout.BeginScrollView (scrollPos, GUILayout.Width (r.width), GUILayout.Height (r.height));
        EditorGUI.indentLevel++;

        EditorGUILayout.PropertyField (resetTerrain);

        showRandom = EditorGUILayout.Foldout (showRandom, new GUIContent("Random","Generates Random Heights"));
        if (showRandom) {
            //
            EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
            GUILayout.Label ("Set Heights Between Random Values", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField (randomHeightRange);
            if (GUILayout.Button ("Random Heights")) {
                terrain.RandomTerrain ();
            }
        }

        showLoadHeights = EditorGUILayout.Foldout (showLoadHeights, new GUIContent("Load Heights","Load heights from a saved texture. Remember to set the texture to read write enabled"));
        if (showLoadHeights) {
            EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
            GUILayout.Label ("Load Heights From Texture", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField (heightMapImage);
            EditorGUILayout.PropertyField (heightMapScale);
            if (GUILayout.Button ("Load Texture")) {
                terrain.LoadTexture ();
            }
        }

        showPerlinNoise = EditorGUILayout.Foldout (showPerlinNoise, new GUIContent("Single Perlin Noise","Generates terrain based on a singular perlin noise"));
        if (showPerlinNoise) {
            EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
            GUILayout.Label ("Perlin Noise", EditorStyles.boldLabel);

            EditorGUILayout.Slider (perlinXScale, 0, 1, new GUIContent ("X Scale", "X scale of the perlin noise"));
            EditorGUILayout.Slider (perlinYScale, 0, 1, new GUIContent ("Y Scale", "Y scale of the perlin noise"));
            EditorGUILayout.IntSlider (perlinOffsetX, 0, 10000, new GUIContent ("Offset X"));
            EditorGUILayout.IntSlider (perlinOffsetY, 0, 10000, new GUIContent ("Offset Y"));
            EditorGUILayout.IntSlider (perlinOctaves, 1, 10, new GUIContent ("Octaves", "Number of octaves for perlin offset"));
            EditorGUILayout.Slider (perlinPersistance, 0.1f, 10, new GUIContent ("Persistance", "The persistance of the perlin noise"));
            EditorGUILayout.Slider (perlinHeightScale, 0, 1, new GUIContent ("Height Scale", "The height scale of the perlin noise"));

            if (GUILayout.Button ("Perlin")) {
                terrain.Perlin ();
            }
        }

        showMultiplePerlin = EditorGUILayout.Foldout (showMultiplePerlin, new GUIContent("Multiple Perlin Noise","Generates  terrain based on multiple perlin noises"));
        if (showMultiplePerlin) {
            EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
            GUILayout.Label ("Mulitple Perlin Noise", EditorStyles.boldLabel);
            perlinParameterTable = GUITableLayout.DrawTable (perlinParameterTable,
                serializedObject.FindProperty ("perlinParameters"));
            EditorGUILayout.Space (20);
            EditorGUILayout.BeginHorizontal ();
            if (GUILayout.Button ("+")) {
                terrain.AddNewPerlin ();
            }
            if (GUILayout.Button ("-")) {
                terrain.RemovePerlin ();
            }
            EditorGUILayout.EndHorizontal ();
            if (GUILayout.Button ("Apply Multiple Perlin")) {
                terrain.MultiplePerlinTerrain ();
            }
        }

        showVoronoi = EditorGUILayout.Foldout (showVoronoi, new GUIContent("Voronoi", "Generates terrain based on Voronoi tesselation"));
        if (showVoronoi) {
            EditorGUILayout.IntSlider (voronoiPeaks, 1, 10, new GUIContent ("Peak Count","Number Of peaks to Generate"));
            EditorGUILayout.Slider (voronoiFallOff, 0, 10, new GUIContent ("Falloff","how steep the slopes become "));
            EditorGUILayout.Slider (voronoiDropOff, 0, 10, new GUIContent ("Dropoff","controls the the drop off value for power calculations "));
            EditorGUILayout.Slider (voronoiMinHeight, 0, 1, new GUIContent ("Min Height"));
            EditorGUILayout.Slider (voronoiMaxHeight, 0, 1, new GUIContent ("Max Height"));
            EditorGUILayout.PropertyField (voronoiType);
            if (GUILayout.Button ("Voronoi")) {
                terrain.Voronoi ();
            }
        }

        showMPD = EditorGUILayout.Foldout (showMPD, new GUIContent("Midpoint Displacement","Generates terrain based on midpoint displacement"));
        if (showMPD) {
            EditorGUILayout.Slider (MPDheightMin, -10, 10, new GUIContent ("Min Height"));
            EditorGUILayout.Slider (MPDheightMax, -10, 10, new GUIContent ("Max Height"));
            EditorGUILayout.PropertyField (MPDheightDampenerPower,new GUIContent("Height Dampener","dampens the height based on roughness"));
            EditorGUILayout.PropertyField (MPDroughness,new GUIContent("Roughness", "determines the smoothness/jaggedness of the terrain "));
            if (GUILayout.Button ("MPD")) {
                terrain.MidPointDisplacement ();
            }
        }

        showSplatMaps = EditorGUILayout.Foldout (showSplatMaps, new GUIContent("Terrain Layers","Paints texture based on height and slope angle"));
        if (showSplatMaps) {

            EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
            GUILayout.Label ("Terrain Layers", EditorStyles.boldLabel);
            splatMapTable = GUITableLayout.DrawTable (splatMapTable, serializedObject.FindProperty ("splatHeights"));
            EditorGUILayout.Space (20);
            EditorGUILayout.BeginHorizontal ();
            if (GUILayout.Button ("+")) {
                terrain.AddNewSplatHeight ();
            }
            if (GUILayout.Button ("-")) {
                terrain.RemoveSplatHeight ();
            }

            EditorGUILayout.EndHorizontal ();
            // EditorGUILayout.Slider (splatNoiseXScale,0.001f,1,new GUIContent("Noise X Scale"));
            //EditorGUILayout.Slider (splatNoiseYScale,0.001f,1,new GUIContent("Noise Y Scale"));
            //EditorGUILayout.Slider (splatNoisescaler,0.001f,1,new GUIContent("Noise Scaler"));
            //EditorGUILayout.Slider (splatOffset, 0.1f, 1, new GUIContent("Splat Offset"));
            if (GUILayout.Button ("Apply Terrain Layers")) {
                terrain.SplatMaps ();
            }
        }

        showVegetation = EditorGUILayout.Foldout (showVegetation, new GUIContent("Vegetation","PLaces Terrain Trees on the map based on height,slope,noise and density"));
        if (showVegetation) {
            EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
            GUILayout.Label ("Vegetation Layers", EditorStyles.boldLabel);
            EditorGUILayout.IntSlider (maxTrees, 0, 100000, new GUIContent ("Max Trees","maximum number of trees spawned"));
            EditorGUILayout.IntSlider (treeSpacing, 2, 20, new GUIContent ("Tree Spacing","the spacing between trees"));
            vegetationTable = GUITableLayout.DrawTable (vegetationTable, serializedObject.FindProperty ("vegetation"));
            EditorGUILayout.Space (20);
            EditorGUILayout.BeginHorizontal ();
            if (GUILayout.Button ("+")) {
                terrain.AddNewVegetation ();
            }
            if (GUILayout.Button ("-")) {
                terrain.RemoveVegetation ();
            }

            EditorGUILayout.EndHorizontal ();
            if (GUILayout.Button ("Apply Vegetation  ")) {
                terrain.PlantVegetation ();
            }

        }
        showDetails = EditorGUILayout.Foldout (showDetails, new GUIContent("Details","places terrain details on the map based height,slope,noise and density"));
        if (showDetails) {
            EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
            EditorGUILayout.IntSlider (maxDetail, 0, 1000, new GUIContent ("Max Detail","Sets the max render distance of the details"));
            EditorGUILayout.IntSlider (detailSpacing, 1, 20, new GUIContent ("Detail Spacing","Spacing of the details"));
            detailTable = GUITableLayout.DrawTable (detailTable, serializedObject.FindProperty ("details"));
            terrain.GetComponent<Terrain> ().detailObjectDistance = maxDetail.intValue;
            EditorGUILayout.Space (20);
            EditorGUILayout.BeginHorizontal ();
            if (GUILayout.Button ("+")) {
                terrain.AddNewDetails ();
            }
            if (GUILayout.Button ("-")) {
                terrain.RemoveDetails ();
            }

            EditorGUILayout.EndHorizontal ();
            if (GUILayout.Button ("Apply Details  ")) {
                terrain.AddDetails ();
            }
        }
        showWater = EditorGUILayout.Foldout (showWater, new GUIContent("Water","Generates a water plane "));
        if (showWater) {
            EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
            GUILayout.Label ("Water", EditorStyles.boldLabel);
            EditorGUILayout.Slider (waterHeight, 0, 1, new GUIContent ("Water Height"));
            EditorGUILayout.PropertyField (waterGO,new GUIContent("Water Prefab","Prefab that is your water plane"));
            if (GUILayout.Button ("Add Water")) {
                terrain.AddWater ();
            }
            EditorGUILayout.PropertyField (shoreLineMaterial,new GUIContent("Shoreline Material","Material used to generate mesh for shoreline"));
            if (GUILayout.Button ("Add Shore Line")) {
                terrain.DrawShoreline ();
            }
        }

        showErosion = EditorGUILayout.Foldout (showErosion, new GUIContent("Erosion","erosion techniques to change the terrain"));
        if (showErosion) {

            EditorGUILayout.PropertyField (erosionType);
            EditorGUILayout.Slider (erosionStrength, 0.001f, 1, new GUIContent("Erosion Strength","How dtrong the erosion is"));
            EditorGUILayout.Slider (erosionAmount, 0.001f, 1, new GUIContent("Erosion Amount","how much it erodes"));
            EditorGUILayout.IntSlider (springsPerRiver, 0, 500, new GUIContent("Droplets","Number of droplets spawned for rain and river "));
            EditorGUILayout.Slider (solubility, 0.001f, 1, new GUIContent("Solubility","solubility of the soil, only used for river"));
            EditorGUILayout.IntSlider (droplets, 0, 20, new GUIContent("Springs Per River","how many branches each river droplet produces"));
            EditorGUILayout.IntSlider (erosionSmoothAmount, 0, 10, new GUIContent("Smooth Amount","How many times the terrain will be smoothed"));
            EditorGUILayout.Slider (WindDir, 0, 360, new GUIContent("Angle of wind","the angle for wind erosion "));

            if (GUILayout.Button ("Erode")) {
                terrain.Erode ();
            }
        }
        showObjects = EditorGUILayout.Foldout (showObjects, new GUIContent("Spawnable Objects","places any game object prefab around the map"));
        if (showObjects) {

            //EditorGUILayout.PropertyField (townMarker1);
            //EditorGUILayout.PropertyField (townMarker2);

            EditorGUILayout.IntSlider (objectSpacing, 5, 500, new GUIContent("Object Spacing","spacing between the objects"));
            SpawnableObjectsTable = GUITableLayout.DrawTable (SpawnableObjectsTable, serializedObject.FindProperty ("objects"));
            EditorGUILayout.Space (20);
            EditorGUILayout.BeginHorizontal ();
            if (GUILayout.Button ("+")) {
                terrain.AddNewSpawnableObjects ();
            }
            if (GUILayout.Button ("-")) {
                terrain.RemoveSpawnableObjects ();
            }
            EditorGUILayout.EndHorizontal ();

            if (GUILayout.Button ("Place Objects")) {

                terrain.PlaceSpawnableObjects ();

            }
        }

        showClouds = EditorGUILayout.Foldout (showClouds, new GUIContent("Clouds","Generates cloud particle system. Warning can be system taxing"));
        if (showClouds) {
            EditorGUILayout.IntSlider(numberOfClouds,0,200,new GUIContent("number Of Clouds","Number of clouds to spawn"));
            EditorGUILayout.IntSlider(particlesPerCloud,0,200,new GUIContent("particles PerC loud","How many particles in the cloud. controls density mainly"));
            EditorGUILayout.IntSlider(cloudParticleSize,0,10,new GUIContent("Particle Size","Size of the particles in the cloud"));
            EditorGUILayout.PropertyField (cloudSizeMin,new GUIContent("Cloud Size Min","The minimum size of the cloud"));
            EditorGUILayout.PropertyField (cloudSizeMax,new GUIContent("Cloud Size Max","The maximum size of the cloud"));
            EditorGUILayout.PropertyField (cloudMaterial);
            //EditorGUILayout.PropertyField (cloudShadowMaterial);
            EditorGUILayout.PropertyField (cloudColour, new GUIContent("Cloud Colour","Colour of the cloud"));
            EditorGUILayout.PropertyField (liningColour, new GUIContent("Cloud Lining Colour","Colour of the clouds lining... haha.. get it clouds have a silver lining"));
            EditorGUILayout.Slider (cloudMinSpeed,0.01f,10, new GUIContent("Cloud Min Speed","Clouds Min Speed"));
            EditorGUILayout.Slider (cloudMaxSpeed,0.01f,10, new GUIContent("Cloud Max Speed","Clouds Max Speed"));
            EditorGUILayout.PropertyField (cloudDistanceTraveled);

            if (GUILayout.Button ("Generate Clouds")) {
                terrain.GenerateClouds ();
            }
        }

        showSmooth = EditorGUILayout.Foldout (showSmooth, "Smooth Terrain");

        if (showSmooth) {
            EditorGUILayout.IntSlider (smoothAmount, 1, 10, new GUIContent ("smoothAmount"));
            if (GUILayout.Button ("Smooth")) {
                terrain.Smooth ();
            }

        }

        EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
        if (GUILayout.Button ("Reset Terrain")) {
            terrain.ResetTerrain ();
        }

        showHeights = EditorGUILayout.Foldout (showHeights, new GUIContent("Height Map","Generates the current height map of the screen"));
        if (showHeights) {

            GUILayout.BeginHorizontal ();
            GUILayout.FlexibleSpace ();
            int hmtSize = (int) (EditorGUIUtility.currentViewWidth - 100);
            GUILayout.Label (hmTexture, GUILayout.Width (hmtSize), GUILayout.Height (hmtSize));
            GUILayout.FlexibleSpace ();
            GUILayout.EndHorizontal ();
            GUILayout.BeginHorizontal ();
            GUILayout.FlexibleSpace ();
            GUILayout.BeginVertical ();
            if (GUILayout.Button ("Refresh", GUILayout.Width (hmtSize))) {
                float[, ] heightMap = terrain.terrainData.GetHeights (0, 0, terrain.terrainData.heightmapResolution, terrain.terrainData.heightmapResolution);
                for (int y = 0; y < terrain.terrainData.heightmapResolution; y++) {
                    for (int x = 0; x < terrain.terrainData.heightmapResolution; x++) {

                        hmTexture.SetPixel (x, y, new Color (heightMap[x, y], heightMap[x, y], heightMap[x, y], 1));
                    }
                }
                hmTexture.Apply ();
            }
            filename = EditorGUILayout.TextField ("Texture Name", filename);
            if (GUILayout.Button ("Save")) {
                byte[] bytes = hmTexture.EncodeToPNG ();
                System.IO.Directory.CreateDirectory (Application.dataPath + "/SavedTextures");
                File.WriteAllBytes (Application.dataPath + "/SavedTextures/" + filename + ".png", bytes);
                AssetDatabase.Refresh ();
            }
            GUILayout.EndVertical ();
            GUILayout.FlexibleSpace ();
            GUILayout.EndHorizontal ();
        }

        EditorGUILayout.EndScrollView ();
        EditorGUILayout.EndVertical ();

        serializedObject.ApplyModifiedProperties ();
    }

}