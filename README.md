# TerrainProcGen



# Procedural Terrain Generation Tool 

## By Joshua Shneier



This tool was made for my Game development elective on **Procedural Generation** i decided to tackle Terrain Generation and made a quite thorough tool for quickly generating terrains for your next URP unity project. in this documents i will give some **Acknowledgement** to those who deserve it.explain the **Process** of creating the tool the **Purpose** and how to **Use the Tool**. 

links to gifs 
https://imgur.com/a/60X7i1k 


## Acknowledgments 
 the back bone of this  tool was based on a purchased Holistic3D Tutorial by Dr Penny de Byl.
 (https://www.udemy.com/share/102c7qAkUfd19SQHw=/). this course provided all the information required to not only work with the Unity Terrain Tool but also how to use varrying Procedural generation techniques for terrain. it gave me a better understanding of perlin generation, midpoint displacement, Voronoi Tessellation ans how to work with the unity terrain class and also the details and tree classes associated with it. In addition it included a great deal of information on Editor Coding which is something that i always struggle to find information on. if you have any interest in these topics i would strongly recommend this course or one of Penny's many other courses. with out this tutorial series this tool would not exist .  



## Process

the flow of the project is evident in the tool structure. 
![tool screenshot](https://imgur.com/a/60X7i1k)

 - it began with generating random heights across the map based on
   noise.
 - then it moved to loading heights from an image
 - then to generating terrain based on Perlin Noise
 - then to generating terrain from multiple Perlins At the same Time.
 - then onto terrain Generation Based on Voronoi Tessellation(which is low key my favorite type).
 -  then on to Mid Point Displacement based on the Square and Diamond Steps.
 - then we began Terrain Painting using spaltmaps based off of the heights and slopes of the terrain. it also makes use of perlin noise to create some bleed between varied textures to make more realistic textures. 
 - then onto vegetation placement based on the height and slope aswell.
 - we used a similar method to also do the Details placement. 
 - water just creates a water plane at a set height. but it also allows for the creation of a shoreline mesh through fidning the terrain that is above and below the water(i included my own water shader and game object to use that creates its own shoreline anyway incase you prefer that).
 - then i began implementing techniques for eroding the landscape to look more natural. Included in the project are :Rain,River,Thermal,Tidal,River, and then Canyon which just cuts straight through your terrain.
 - after that i added cloud generation through a particle system. 
 - then lastly i added the Spawnable objects which allows you to procedurally place any prefab across the map based on height and slope for what ever reason you want. 

There is also an added a seamless noise texture creator and smoothing functionality. i also added  the ability to save out the current height map texture for future use.
	 

## Purpose

I created this tool to allow for the rapid prototyping of terrain. the idea behind using it is that the user would use this tool generate terrain textures and trees place objects and details and then go in for fine tuning and building placement.  in a future update i intend to include path generation based on the users building placement. this tool would allow for people to generate realistic terrain relatively quickly for large scale maps. it also allows for the great generation of mountain textures and could be used to generate Landmarks of interest for players to view in game on larger maps. 

## Use The Tool
please note all values in the tool are toolTipped so you can what the values change in your script

To start you can either use the projects base scene or import  the Package.

when that's done create a new terrain object and add the CustomTerrain.cs component
![1](https://imgur.com/77JOsPd)

### functions:
**Random ** will generate random heights between your inputted values.

![Random](https://imgur.com/nAnEXXK)

** Load Heights** allows the user to load in an image as a height map. NB Make sure the image is set to Read write enabled.
![LoadHeights](https://imgur.com/jUXLm4Z)


**Single Perlin Noise** will generate terrain based on a singular perlin noise.

![Single perlin](https://imgur.com/xcL8U5n)


**Multiple Perlin** Noise will Generate terrain Based on Multiple Perlin Noises

![Multiple perlin](https://imgur.com/ne8Ldhh)

**Voronoi** Uses varied formulas to generate generate terrain.

![vornoi](https://imgur.com/ifTos9Q)

**Midpoint Displacement** generates terrain based on MPD calculations.

![MPD](https://imgur.com/FgWqEFe)

**Terrain layer** will generate splat maps based on the height and slope of the terrain. it also applies perlin noise to offset the terrain to blend better.

![spaltmaps](https://imgur.com/06dwmp9)

**Vegetation** will generate trees based on heights and slopes values.

![tree](https://imgur.com/rxdQ5us)

**Details** places terrain details on the map it allows for both objects and textures for grass billboards.

[details](https://imgur.com/GEK2dvo)

**Water** will place a water gameObject and also allows you to generate a shoreline based off of a texture

[water](https://imgur.com/rli2fFA)

### NB when using erosion or the smooth function please switch off the reset toggle at the top of the tool. you can also switch it off to apply multiple Generational tools additively.

![reset](https://imgur.com/LCDde4U)

**Erosion** allows for you to use varied types of erosion to change your land scape in a natural manor
**rain**

![rain](https://imgur.com/1h2BQZf)

**river**

![river](https://imgur.com/MhfCi2n)

**thermal**

![thermal](https://imgur.com/4b4OGDH)


**tidal**

![tidal](https://imgur.com/gllBQkR)

**wind**

![wind](https://imgur.com/P5I2N1S)

**canyon**

![Canyon](https://imgur.com/l7JOoul)

just note the caynon kinda does what ever it wants and has a long load time so be cautious

**Spawnable Objects** allows you to place game objects based on slope and terrain height.

![spo](https://imgur.com/CR7YCDP)

**Clouds** are a two step Proccess first they set up a cloud manager that the user can position and scale and then you can generate clouds from that point 

![Clouds](https://imgur.com/f62nbNT)


# thanks for the download and i hope you enjoy 
> Written with [StackEdit](https://stackedit.io/).